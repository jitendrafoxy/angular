import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutsComponent } from './abouts/abouts.component';
import { ContactComponent } from './contact/contact.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { NewsComponent } from './news/news.component';
import { BlogDetailsComponent } from './blog-details/blog-details.component';

//const routes: Routes = [];
const routes: Routes = [
{
  path:'',
  component:HomeComponent
},
{
  path:'blog',
  component:AboutsComponent
},
{
  path:'blogdetail',
  component:BlogDetailsComponent
},
 {
  path:'contact',
  component:ContactComponent
},
 {
  path:'sitemap',
  component:SitemapComponent
},
{
  path:'news',
  component:NewsComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
